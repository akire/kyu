<?php

namespace Database\Factories;

use App\Models\Order;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'user_id' => User::factory(),
            'status' => $this->faker->randomElement(['PENDING', 'DONE']),
            'code' => $this->faker->ean13,
            'date_paid' => $this->faker->dateTime($max = 'now', $timezone = null) 
        ];
    }
}
