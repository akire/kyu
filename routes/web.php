<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware('auth')->group(function() {

	// Users CRUD
	Route::get('/users', [App\Http\Controllers\UserController::class, 'index'])->name('users.index');
	Route::get('/users/create', [App\Http\Controllers\UserController::class, 'create'])->name('user.create');
	Route::post('/users/store', [App\Http\Controllers\UserController::class, 'store'])->name('user.store');
	Route::get('/users/{user}/edit', [App\Http\Controllers\UserController::class, 'edit'])->name('user.edit');
	Route::put('/users/{user}/update', [App\Http\Controllers\UserController::class, 'update'])->name('user.update');
	Route::delete('/users/{user}/delete', [App\Http\Controllers\UserController::class, 'destroy'])->name('user.delete');

	// Orders CRUD
	Route::get('/orders', [App\Http\Controllers\OrderController::class, 'index'])->name('orders.index');
	Route::get('/orders/{user}', [App\Http\Controllers\OrderController::class, 'displayUserOrders'])->name('user.orders.show');
	Route::post('/users/{user}/orders', [App\Http\Controllers\OrderController::class, 'store'])->name('order.store');
	Route::delete('/orders/{order}', [App\Http\Controllers\OrderController::class, 'destroy'])->name('order.destroy');
	Route::get('/users/{user}/orders', [App\Http\Controllers\OrderController::class, 'showUserOrders'])->middleware('can:view,user')->name('user.orders.index');
	Route::patch('/orders/{order}/update', [App\Http\Controllers\OrderController::class, 'update'])->name('user.order.update');
	// Route::post('/users/store', [App\Http\Controllers\UserController::class, 'store'])->name('user.store');
	// Route::get('/users/{user}/edit', [App\Http\Controllers\UserController::class, 'edit'])->name('user.edit');
	// Route::put('/users/{user}/update', [App\Http\Controllers\UserController::class, 'update'])->name('user.update');
	// Route::delete('/users/{user}/delete', [App\Http\Controllers\UserController::class, 'destroy'])->name('user.delete');

});
