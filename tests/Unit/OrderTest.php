<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;
use App\Models\Order;


class OrderTest extends TestCase
{
	use RefreshDatabase;
    
    /** @test */
    public function it_belongs_to_a_user()
    {
    	$order = Order::factory()->create();
        $this->assertInstanceOf('App\Models\User', $order->user);
    }
}
