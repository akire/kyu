@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Users') }}</div>
                <div class = "row">
                  <div class = "col-sm-3">
                    <a href = "{{ route('user.create') }}" class = "btn btn-success pull-right">Add New User</a>
                  </div>
                </div>
                
                @if(session('message'))
                  <div class = "alert alert-success">{{ session('message') }}</div>
                @endif

                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Id</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Contact Number</th>
                          <th>Created At</th>
                          <th>Actions</th>
                          <th>Orders</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($users as $user)
                        <tr>
                          <td>{{ $user->id }}</td>
                          <td>{{ $user->name }}</td>
                          <td>{{ $user->email }}</td>
                          <td>{{ $user->contact_number }}</td>
                          <td>{{ $user->created_at->diffForHumans() }}</td>
                          <td>
                            <a href = "{{ route('user.edit', $user) }}" class = "btn btn-warning btn-block btn-sm">Edit</a><br>
                            <form action = "{{ route('user.delete', $user) }}" method = "POST">
                              @csrf
                              @method('DELETE')
                              <button type = "submit" class = "btn btn-danger btn-block btn-sm">Delete</button>
                            </form>
                          </td>
                          <td><a href = "{{ route('user.orders.index', $user) }}" class = "btn btn-primary">Orders</a></td>
                          {{-- <td>
                            @can('delete', $post)
                                <form action = "{{ route('post.destroy', $post->id) }}" method = "POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type = "submit" class = "btn btn-danger">Delete</button>
                                </form>
                            @endcan
                          </td> --}}
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/demo/datatables-demo.js') }}"></script>
@endsection