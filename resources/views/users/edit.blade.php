@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit User') }}</div>

                <div class="card-body">
                     @if(session('message'))
                      <div class = "alert alert-success">{{ session('message') }}</div>
                    @endif


                    <form action = "{{ route('user.update', $user) }}" method = "POST">
                        @csrf
                        @method('PUT')

                        <div class = "form-group">
                            <label for = "name">Name</label>
                            <input type = "text" name = "name" class = "form-control" value = "{{ $user->name }}" required>

                            @error('name')
                                <div class = "alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class = "form-group">
                            <label for = "email">Email</label>
                            <input type = "email" name = "email" class = "form-control" value = "{{ $user->email }}" required>

                            @error('email')
                                <div class = "alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class = "form-group">
                            <label for = "contact_number">Contact Number</label>
                            <input type = "text" name = "contact_number" class = "form-control" value = "{{ $user->contact_number }}" required>

                            @error('contact_number')
                                <div class = "alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class = "form-group">
                            <label for = "password">Password</label>
                            <input type = "password" name = "password" class = "form-control" >

                            @error('password')
                                <div class = "alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class = "form-group">
                            <label for = "password_confirmation">Password Confirmation</label>
                            <input type = "password" name = "password_confirmation" class = "form-control" >

                            @error('password_confirmation')
                                <div class = "alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <button type = "submit" class = "btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
