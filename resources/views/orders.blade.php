@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Orders') }}</div>
                
                @if(session('message'))
                  <div class = "alert alert-success">{{ session('message') }}</div>
                @endif
                <div class="card-body">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Id</th>
                          <th>Code</th>
                          <th>Remarks</th>
                          <th>Name</th>
                          <th>Created At</th>
                          <th>Updated At</th>
                          <th>Status</th>
                          <th>Update Status</th>
                          <th>Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($orders as $order)
                        <tr>
                          <td>{{ $order->id }}</td>
                          <td>{{ $order->code }}</td>
                          <td>{{ $order->remarks }}</td>
                          <td><a href = "{{ route('user.orders.index', $order->user) }}">{{ $order->user->name }}</a></td>
                          <td>{{ $order->created_at->diffForHumans() }}</td>
                          <td>{{ $order->updated_at->diffForHumans() }}</td>
                          <td>{{ $order->status }}</td>
                          <td>
                            <form action = "{{ route('user.order.update', $order) }}" method = "POST">
                              @csrf
                              @method('PATCH')
                              <select name = "status" class = "form-control" onChange = "this.form.submit()">
                                <option></option>
                                <option>PENDING</option>
                                <option>DONE</option>
                              </select>
                            </form>
                          </td>
                          <td>
                              <form action = "{{ route('order.destroy', $order) }}" method = "POST">
                                  @csrf
                                  @method('DELETE')
                                  <button type = "submit" class = "btn btn-danger">Delete</button>
                              </form>
                            {{-- <a href = "{{ route('user.edit', $user) }}" class = "btn btn-warning btn-block btn-sm">Edit</a><br> --}}
                            {{-- <form action = "{{ route('user.delete', $user) }}" method = "POST">
                              @csrf
                              @method('DELETE')
                              <button type = "submit" class = "btn btn-danger btn-block btn-sm">Delete</button>
                            </form> --}}
                          </td>
                          {{-- <td>
                            @can('delete', $post)
                                <form action = "{{ route('post.destroy', $post->id) }}" method = "POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type = "submit" class = "btn btn-danger">Delete</button>
                                </form>
                            @endcan
                          </td> --}}
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    {{ $orders->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/demo/datatables-demo.js') }}"></script>
@endsection