@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-3">
            <div class="card">
                <div class="card-header">New Order</div>
                
                <div class="card-body">
                  <form action = "{{ route('order.store', $user) }}" method = "POST">
                    @csrf
                    <div class = "form-group">
                      <label for = "date_paid">Date Paid</label>
                      <input type = "date" name = "date_paid" class = "form-control" required>
                    </div>

                    <div class = "form-group">
                      <label for = "remarks">Remarks</label>
                      <input type = "text" name = "remarks" class = "form-control" required>
                    </div>

                    <button type = "submit" class ="btn btn-primary">Create</button>
                  </form>
                </div>

              </div>
        </div>

        <div class="col-sm-9">
            <div class="card">
                <div class="card-header">Orders of {{ $user->name }}</div>
                

                <div class="card-body">
                    @if(session('message'))
                      <div class = "alert alert-success">{{ session('message') }}</div>
                    @endif


                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Id</th>
                          <th>Code</th>
                          <th>Remarks</th>
                          <th>Created At</th>
                          <th>Date Paid</th>
                          <th>Status</th>
                          <th>Update Status</th>
                          <th>Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($orders as $order)
                        <tr>
                          <td>{{ $order->id }}</td>
                          <td>{{ $order->code }}</td>
                          <td>{{ $order->remarks }}</td>
                          <td>{{ date('M j, Y h:i a', strtotime($order->created_at)) }}</td>
                          <td>{{ date('M j, Y h:i a', strtotime($order->date_paid)) }}</td>
                          <td>{{ $order->status }}</td>
                          <td>
                            {{-- <a href = "{{ route('user.edit', $user) }}" class = "btn btn-warning btn-block btn-sm">Edit</a><br> --}}
                            <form action = "{{ route('user.order.update', $order) }}" method = "POST">
                              @csrf
                              @method('PATCH')
                              <select name = "status" class = "form-control" onChange = "this.form.submit()">
                                <option></option>
                                <option>PENDING</option>
                                <option>DONE</option>
                              </select>
                            </form>
                          </td>
                          <td>
                              <form action = "{{ route('order.destroy', $order) }}" method = "POST">
                                  @csrf
                                  @method('DELETE')
                                  <button type = "submit" class = "btn btn-danger">Delete</button>
                              </form>
                          </td>
                          {{-- <td>
                            @can('delete', $post)
                                
                            @endcan
                          </td> --}}
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    {{ $orders->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/demo/datatables-demo.js') }}"></script>
@endsection