@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">My Ordersssss</div>
                

                <div class="card-body">
                    @if(session('message'))
                      <div class = "alert alert-success">{{ session('message') }}</div>
                    @endif


                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Id</th>
                          <th>Code</th>
                          <th>Remarks</th>
                          <th>Created At</th>
                          <th>Date Paid</th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($orders as $order)
                        <tr>
                          <td>{{ $order->id }}</td>
                          <td>{{ $order->code }}</td>
                          <td>{{ $order->remarks }}</td>
                          <td>{{ date('M j, Y h:i a', strtotime($order->created_at)) }}</td>
                          <td>{{ date('M j, Y h:i a', strtotime($order->date_paid)) }}</td>
                          <td><label class = "alert {{ $order->status == 'PENDING' ? 'alert-warning' : 'alert-success' }} btn-block"><center>{{ $order->status }}</center></label></td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    {{ $orders->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/demo/datatables-demo.js') }}"></script>
@endsection