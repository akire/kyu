<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(auth()->user()->role == NULL || auth()->user()->role->name == 'Subscriber'){

            $orders = auth()->user()->orders()->paginate(10);
            return view('orders.userOrders', ['orders' => $orders]);
        }

        return view('home');
    }
}
