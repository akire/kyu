<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('orders', [ 'orders' => Order::paginate(10) ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $attributes = request()->validate([
            'date_paid' => 'required',
            'remarks' => 'required|max:255'
        ]);

        $this->authorize('create', $user);
        $user->orders()->create([
            'code' => $this->generateBarcodeNumber(),
            'date_paid' => $attributes['date_paid'],
            'remarks' => $attributes['remarks']
        ]);

        session()->flash('message', 'New Order created for:'. $user->name);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function showUserOrders(User $user)
    {
        //

        $orders = $user->orders()->paginate(10);
        return view('orders.index', [ 'orders' => $orders, 'user' => $user] );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function displayUserOrders(User $user)
    {
        //

        $orders = auth()->user()->orders()->paginate(10);

        // dd(auth()->user());
        // return "hello";
        return view('orders.userOrders', [ 'orders' => $orders] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
        $this->authorize('update',$order);
        $order->status = request('status');
        $order->save();
        session()->flash('message', 'Order Status updated');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
        $this->authorize('delete', $order);
        $order->delete();
        session()->flash('message', 'Order deleted');
        return back();
    }

    protected function generateBarcodeNumber() {

        do{
            $number = mt_rand(1000000000, 9999999999);
        } while($this->barcodeNumberExists($number));

        // otherwise, it's valid and can be used
        return $number;
    }

    protected function barcodeNumberExists($code) {
        // query the database and return a boolean
        // for instance, it might look like this in Laravel
        return Order::whereCode($code)->exists();
    }
}
