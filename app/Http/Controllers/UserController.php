<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users  = User::where('role_id', '<>', 1)->orWhereNull('role_id')->paginate(10);
        return view('users.index', [ 'users' => $users ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $attributes = request()->validate([
            'name' => 'required|max:255',
            'email' => 'required|unique:users',
            'contact_number' => 'required|unique:users|max:11',
            'password' => 'confirmed'
        ]);


        $user = User::create($attributes);
        session()->flash('message', 'User created');
        $orders = $user->orders();


        return redirect()->route('user.orders.index', [ 'orders' => $orders, 'user' => $user]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
        return view('users.edit', [ 'user' => $user ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
        $attributes = request()->validate([
            'name' => 'required|max:255',
            'email' => 'required',
            'contact_number' => 'required|max:11'
        ]);

        if(request('password')){
            $attributes['password'] = request('password');
        }


        $user->update($attributes);
        session()->flash('message', 'User updated');

        return redirect()->route('user.edit', $user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
        $user->delete();
        session()->flash('message', 'User deleted');
        return back();
    }
}
